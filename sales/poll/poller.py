import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

from sales_rest.models import AutomobileVO

def poll(repeat=True):
    while True:
        print('Sales poller polling for data')
        try:
            url = "http://project-beta-inventory-api-1:8000/api/automobiles/"
            response = requests.get(url)
            content = json.loads(response.content)
            for automobile in content["autos"]:
                import_href = automobile["href"]
                vin = automobile["vin"]
                sold = automobile["sold"]

                existing_automobile = AutomobileVO.objects.filter(import_href=import_href).first()

                if not existing_automobile:
                    AutomobileVO.objects.create(
                        import_href=import_href,
                        vin=vin,
                        sold=sold,
                    )

        except Exception as e:
            print(e, file=sys.stderr)
        
        if (not repeat):
            break

        time.sleep(60)


if __name__ == "__main__":
    poll()
