from django.urls import path
from .views import (
    api_salesperson_list,
    api_salesperson_delete,
    api_customer_list,
    api_customer_delete,
    api_sale_list,
    api_sale_delete,
    )


urlpatterns = [
    path('salespeople/', api_salesperson_list, name='api_salesperson_list'),
    path('salespeople/<int:pk>/', api_salesperson_delete, name='api_salesperson_delete'),
    path('customers/', api_customer_list, name='api_customer_list'),
    path('customers/<int:pk>/', api_customer_delete, name='api_customer_delete'),
    path('sales/', api_sale_list, name='api_sale_list'),
    path('sales/<int:pk>/', api_sale_delete, name='api_sale_delete'),
]