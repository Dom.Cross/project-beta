from django.contrib import admin
from .models import Sale, Customer, Salesperson, AutomobileVO


admin.site.register(Sale)
admin.site.register(Customer)
admin.site.register(Salesperson)
admin.site.register(AutomobileVO)
