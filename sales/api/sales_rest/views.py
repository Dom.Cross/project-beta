from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponseNotAllowed
from django.views.decorators.http import require_http_methods
from django.core.exceptions import ObjectDoesNotExist
from common.json import ModelEncoder
from .models import AutomobileVO, Salesperson, Customer, Sale
import json


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "automobile",
        "salesperson",
        "customer",
        "price",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_salesperson_list(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
            )
    
    elif request.method == 'POST':
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False
            )


@require_http_methods(["DELETE"])
def api_salesperson_delete(request, pk):
    salesperson = get_object_or_404(Salesperson, id=pk)
    salesperson.delete()
    return JsonResponse(
        {'message': 'Salesperson deleted successfully'},
        )


@require_http_methods(["GET", "POST"])
def api_customer_list(request):
    if request.method == 'GET':
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
            safe=False
            )

    elif request.method == 'POST':
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
            )


@require_http_methods(["DELETE"])
def api_customer_delete(request, pk):
    customer = get_object_or_404(Customer, id=pk)
    customer.delete()
    return JsonResponse(
        {'message': 'Customer deleted successfully'},
        )


@require_http_methods(["GET", "POST"])
def api_sale_list(request):
    if request.method == 'GET':
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
            safe=False
            )

    elif request.method == 'POST':
        content = json.loads(request.body)
        automobile_vin = content.get("automobile")
        salesperson_id = content.get("salesperson")
        customer_id = content.get("customer")
        price = content.get("price")

        try:
            automobile = AutomobileVO.objects.get(vin=automobile_vin)

            if automobile.sold:
                response = JsonResponse(
                    {"message": "Automobile already sold."},
                    status=400
                )
                return response
            
            salesperson = Salesperson.objects.get(id=salesperson_id)
            customer = Customer.objects.get(id=customer_id)

            sale = Sale.objects.create(
                automobile=automobile,
                salesperson=salesperson,
                customer=customer,
                price=price,
            )

            automobile.sold = True
            automobile.save()

            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False
            )

        except ObjectDoesNotExist as e:
            response = JsonResponse(
                {"message": "One or more entities do not exist."},
                status=400
            )
            return response

        except Exception as e:
            response = JsonResponse(
                {"message": "An error occurred while creating the sale."},
                status=500
            )
            return response


@require_http_methods(["DELETE"])
def api_sale_delete(request, pk):
    sale = get_object_or_404(Sale, id=pk)
    sale.delete()
    return JsonResponse(
        {'message': 'Sale deleted successfully'},
        )
