import React, { useEffect, useState } from "react";

function SalesPeople() {
    const [salespeopleData, setSalespeopleData] = useState([]);

    useEffect(() => {
        async function fetchData() {
            try {
                const url = "http://localhost:8090/api/salespeople/";
                const response = await fetch(url);
                if (!response.ok) {
                    alert("Bad response!");
                } else {
                    const data = await response.json();
                    setSalespeopleData(data.salespeople);
                }
            } catch (e) {
                alert("Error was raised!")
            }
        }

        fetchData();
    }, []);

    return (
        <div>
            <h1 style={{ fontSize: "42px", paddingTop: "30px", paddingBottom: "10px" }}>Salespeople</h1>
            <table style={{ width: "100%", borderCollapse: "collapse" }}>
                <thead>
                    <tr>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Employee ID</th>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>First Name</th>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeopleData.map((salesperson, index) => (
                        <tr key={index} style={{ backgroundColor: index % 2 === 1 ? "#f2f2f2" : "white" }}>
                            <td>{salesperson.employee_id}</td>
                            <td>{salesperson.first_name}</td>
                            <td>{salesperson.last_name}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default SalesPeople;