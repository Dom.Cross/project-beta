import React, { useState, useEffect } from "react";

function AddaSale() {
  const [formData, setFormData] = useState({
    automobile: "",
    salesperson: "",
    customer: "",
    price: 0,
  });

  const [automobiles, setAutomobiles] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [selectedAutomobile, setSelectedAutomobile] = useState(null);

  const handleSubmit = async (event) => {
    event.preventDefault();
    setIsLoading(true);

    if (!selectedAutomobile) {
      setErrorMessage("Please select an automobile.");
      setIsLoading(false);
      return;
    }

    const saleUrl = "http://localhost:8090/api/sales/";
    const automobileUrl = `http://localhost:8100/api/automobiles/${formData.automobile}/`;
    const saleFetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    try {
      const automobileUpdateConfig = {
        method: "PUT",
        body: JSON.stringify({
          ...selectedAutomobile,
          sold: true,
        }),
        headers: {
          "Content-Type": "application/json",
        },
      };
      await fetch(automobileUrl, automobileUpdateConfig);

      const response = await fetch(saleUrl, saleFetchConfig);

      if (response.ok) {
        setSuccessMessage("Sale recorded successfully!");
        setFormData({
          automobile: "",
          salesperson: "",
          customer: "",
          price: 0,
        });

        setAutomobiles((prevAutomobiles) =>
          prevAutomobiles.filter((auto) => auto.vin !== formData.automobile)
        );
      } else {
        setErrorMessage("Failed to record the sale.");
      }
    } catch (e) {
      setErrorMessage("Error occurred while recording the sale.");
    } finally {
      setIsLoading(false);
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });

    if (inputName === "automobile") {
      const automobileData = automobiles.find((auto) => auto.vin === value);
      setSelectedAutomobile(automobileData);
    }
  };

  useEffect(() => {
    async function fetchAutomobiles() {
      try {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error("Bad response while fetching automobiles!");
        }
        const data = await response.json();
        const unsoldAutomobiles = data.autos.filter((auto) => !auto.sold);
        setAutomobiles(unsoldAutomobiles);
      } catch (e) {
        setErrorMessage("Error occurred while fetching automobiles.");
      }
    }

    async function fetchSalespeople() {
      try {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error("Bad response while fetching salespeople!");
        }
        const data = await response.json();
        setSalespeople(data.salespeople);
      } catch (e) {
        setErrorMessage("Error occurred while fetching salespeople.");
      }
    }

    async function fetchCustomers() {
      try {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error("Bad response while fetching customers!");
        }
        const data = await response.json();
        setCustomers(data.customers);
      } catch (e) {
        setErrorMessage("Error occurred while fetching customers.");
      }
    }

    fetchAutomobiles();
    fetchSalespeople();
    fetchCustomers();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          {errorMessage && <div className="alert alert-danger">{errorMessage}</div>}
          {successMessage && <div className="alert alert-success">{successMessage}</div>}
          <form onSubmit={handleSubmit} id="record-sale-form">
            <div className="mb-3">
              <label htmlFor="automobile">Automobile VIN</label>
              <select onChange={handleFormChange} required name="automobile" id="automobile" className="form-select">
                <option value="">Choose an automobile VIN</option>
                {automobiles.map((auto) => (
                  <option key={auto.id} value={auto.vin}>
                    {auto.vin}
                  </option>
                ))}
              </select>
            </div>

            <div className="mb-3">
              <label htmlFor="salesperson">Salesperson</label>
              <select onChange={handleFormChange} required name="salesperson" id="salesperson" className="form-select">
                <option value="">Choose a salesperson</option>
                {salespeople.map((person) => (
                  <option key={person.id} value={person.id}>
                    {person.first_name} {person.last_name}
                  </option>
                ))}
              </select>
            </div>

            <div className="mb-3">
              <label htmlFor="customer">Customer</label>
              <select onChange={handleFormChange} required name="customer" id="customer" className="form-select">
                <option value="">Choose a customer</option>
                {customers.map((customer) => (
                  <option key={customer.id} value={customer.id}>
                    {customer.first_name} {customer.last_name}
                  </option>
                ))}
              </select>
            </div>

            <div className="mb-3">
              <label htmlFor="price">Price</label>
              <input
                onChange={handleFormChange}
                placeholder="Price"
                required
                type="number"
                step="1"
                name="price"
                id="price"
                className="form-control"
                value={formData.price}
              />
            </div>

            <button className="btn btn-primary" disabled={isLoading}>
              {isLoading ? "Recording..." : "Record Sale"}
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AddaSale;