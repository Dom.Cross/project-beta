import React, { useState, useEffect } from "react";

function CreateVehicleModel() {
  const [formData, setFormData] = useState({
    name: "",
    picture_url: "",
    manufacturer: "",
  });

  const [manufacturers, setManufacturers] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    setIsLoading(true);
    const url = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setSuccessMessage("Model created successfully!");
        setFormData({
          name: "",
          picture_url: "",
          manufacturer: "",
        });
      } else {
        setErrorMessage("Failed to create the model.");
      }
    } catch (e) {
      setErrorMessage("Error occurred while creating the model.");
    } finally {
      setIsLoading(false);
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    const updatedFormData = {
      ...formData,
      [inputName === "manufacturer" ? "manufacturer_id" : inputName]: inputName === "manufacturer" ? parseInt(value) : value,
    };
  
    setFormData(updatedFormData);
  };

  useEffect(() => {
    async function fetchManufacturers() {
      try {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error("Bad response while fetching manufacturers!");
        }
        const data = await response.json();
        setManufacturers(data.manufacturers);
      } catch (e) {
        setErrorMessage("Error occurred while fetching manufacturers.");
      }
    }

    fetchManufacturers();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a vehicle model</h1>
          {errorMessage && <div className="alert alert-danger">{errorMessage}</div>}
          {successMessage && <div className="alert alert-success">{successMessage}</div>}
          <form onSubmit={handleSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={formData.name} />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" value={formData.picture_url} />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
              <label htmlFor="manufacturer">Manufacturer</label>
              <select onChange={handleFormChange} required name="manufacturer" id="manufacturer" className="form-select">
                <option value="">Choose a manufacturer</option>
                {manufacturers.map(manufacturer => {
                  return (
                    <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary" disabled={isLoading}>
              {isLoading ? "Creating..." : "Create Model"}
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateVehicleModel;