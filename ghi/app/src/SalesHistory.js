import React, { useEffect, useState } from "react";

function SalesHistory() {
    const [salesData, setSalesData] = useState([]);
    const [salespeopleData, setSalespeopleData] = useState([]);
    const [selectedSalesperson, setSelectedSalesperson] = useState("");

    useEffect(() => {
        async function fetchSalesData() {
            try {
                const url = "http://localhost:8090/api/sales/";
                const response = await fetch(url);
                if (!response.ok) {
                    alert("Bad response!");
                } else {
                    const data = await response.json();
                    setSalesData(data.sales);
                }
            } catch (e) {
                alert("Error was raised!");
            }
        }

        async function fetchSalespeopleData() {
            try {
                const url = "http://localhost:8090/api/salespeople/";
                const response = await fetch(url);
                if (!response.ok) {
                    alert("Bad response!");
                } else {
                    const data = await response.json();
                    setSalespeopleData(data.salespeople);
                }
            } catch (e) {
                alert("Error was raised!");
            }
        }

        fetchSalesData();
        fetchSalespeopleData();
    }, []);

    const handleSalespersonChange = (event) => {
        setSelectedSalesperson(event.target.value);
    };

    const filteredSalesData = salesData.filter(
        (sale) =>
            selectedSalesperson === "" ||
            `${sale.salesperson.first_name} ${sale.salesperson.last_name}` === selectedSalesperson
    );

    return (
        <div>
            <h1 style={{ fontSize: "42px", paddingTop: "30px", paddingBottom: "10px" }}>Salesperson History</h1>
            <div style={{ marginBottom: "20px" }}>
                <label htmlFor="salespersonSelect">Select Salesperson: </label>
                <select
                    id="salespersonSelect"
                    onChange={handleSalespersonChange}
                    value={selectedSalesperson}
                >
                    <option value="">Select</option>
                    {salespeopleData.map((salesperson) => (
                        <option key={salesperson.employee_id} value={`${salesperson.first_name} ${salesperson.last_name}`}>
                            {salesperson.first_name} {salesperson.last_name}
                        </option>
                    ))}
                </select>
            </div>
            <table style={{ width: "100%", borderCollapse: "collapse" }}>
                <thead>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Salesperson</th>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Customer</th>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>VIN</th>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Price</th>
                </thead>
                <tbody>
                    {selectedSalesperson === "" ? (
                        <tr>
                            <td colSpan="4">Select a Salesperson to view sales data</td>
                        </tr>
                    ) : (
                        filteredSalesData.map((sales, index) => (
                            <tr key={index} style={{ backgroundColor: index % 2 === 1 ? "#f2f2f2" : "white" }}>
                                <td>{sales.salesperson.first_name} {sales.salesperson.last_name}</td>
                                <td>{sales.customer.first_name} {sales.customer.last_name}</td>
                                <td>{sales.automobile.vin}</td>
                                <td>${sales.price}</td>
                            </tr>
                        ))
                    )}
                </tbody>
            </table>
        </div>
    );
}

export default SalesHistory;