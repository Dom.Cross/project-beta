import React, { useState, useEffect } from "react";

function CreateAutomobile() {
  const [formData, setFormData] = useState({
    color: "",
    year: "",
    vin: "",
    model: "",
});

  const [models, setModels] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    setIsLoading(true);
    const url = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setSuccessMessage("Automobile created successfully!");
        setFormData({
          color: "",
          year: "",
          vin: "",
          model: "",
        });
      } else {
        setErrorMessage("Failed to create the automobile.");
      }
    } catch (e) {
      setErrorMessage("Error occurred while creating the automobile.");
    } finally {
      setIsLoading(false);
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    const updatedFormData = {
      ...formData,
      [inputName === "model" ? "model_id" : inputName]: inputName === "model" ? parseInt(value) : value,
    };
  
    setFormData(updatedFormData);
  };

  useEffect(() => {
    async function fetchModels() {
      try {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error("Bad response while fetching models!");
        }
        const data = await response.json();
        setModels(data.models);
      } catch (e) {
        setErrorMessage("Error occurred while fetching models.");
      }
    }

    fetchModels();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add an automobile to inventory</h1>
          {errorMessage && <div className="alert alert-danger">{errorMessage}</div>}
          {successMessage && <div className="alert alert-success">{successMessage}</div>}
          <form onSubmit={handleSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={formData.color} />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Year" required type="number" min="1000" max="9999" name="year" id="year" className="form-control" value={formData.year} />
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="VIN" required type="text" minLength="17" maxLength="17" name="vin" id="vin" className="form-control" value={formData.vin} />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="mb-3">
              <label htmlFor="model">Model</label>
              <select onChange={handleFormChange} required name="model" id="model" className="form-select">
                <option value="">Choose a model</option>
                {models.map(model => {
                  return (
                    <option key={model.id} value={model.id}>{model.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary" disabled={isLoading}>
              {isLoading ? "Creating..." : "Create Automobile"}
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateAutomobile;