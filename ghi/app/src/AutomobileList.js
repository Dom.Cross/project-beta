import React from 'react';

class AutomobileList extends React.Component {
    constructor(props){
        super(props);
        this.state={
            autos: [],
        };
    }

    async componentDidMount() {
        const URL = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(URL);
        if (response.ok) {
            const data = await response.json();
            this.setState({ autos: data.autos });
        }
    }
    render() {
        return (
        <div className="shadow p-4 mt-4">
        <h1 style={{ fontSize: "42px", paddingTop: "30px", paddingBottom: "10px" }}>Automobiles</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Vin #</th>
                    <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Color</th>
                    <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Year</th>
                    <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Model</th>
                    <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Manufacturer</th>
                    <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Sold</th>
                </tr>
            </thead>
        <tbody>
           {this.state.autos.map((autos) => {
               return (
                   <tr key={ autos.href }>
                        <td>{ autos.vin }</td>
                        <td>{ autos.color }</td>
                        <td>{ autos.year }</td>
                        <td>{ autos.model.name }</td>
                        <td>{ autos.model.manufacturer.name }</td>
                        <td>{ autos.sold }</td>
                    </tr>
            );
        })}
        </tbody>
      </table>
    </div>
    );
  }
}

export default AutomobileList
