import React, { useState } from "react";

function AddaSalesperson() {
  const [formData, setFormData] = useState({
    first_name: "",
    last_name: "",
    employee_id: "",
  });

  const [isLoading, setIsLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    setIsLoading(true);

    const url = "http://localhost:8090/api/salespeople/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setSuccessMessage("Salesperson added successfully!");
        setFormData({
          first_name: "",
          last_name: "",
          employee_id: "",
        });
      } else {
        setErrorMessage("Failed to add the salesperson.");
      }
    } catch (e) {
      setErrorMessage("Error occurred while adding the salesperson.");
    } finally {
      setIsLoading(false);
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Salesperson</h1>
          {errorMessage && <div className="alert alert-danger">{errorMessage}</div>}
          {successMessage && <div className="alert alert-success">{successMessage}</div>}
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-1">
              <input onChange={handleFormChange} placeholder="First name" required type="text" name="first_name" id="first_name" className="form-control" value={formData.first_name} />
              <label htmlFor="first_name">First name...</label>
            </div>
            <div className="form-floating mb-1">
              <input onChange={handleFormChange} placeholder="Last name" required type="text" name="last_name" id="last_name" className="form-control" value={formData.last_name} />
              <label htmlFor="last_name">Last name...</label>
            </div>
            <div className="form-floating mb-4">
              <input onChange={handleFormChange} placeholder="Employee id" required type="text" name="employee_id" id="employee_id" className="form-control" value={formData.employee_id} />
              <label htmlFor="employee_id">Employee id...</label>
            </div>
            <button className="btn btn-primary" disabled={isLoading}>
              {isLoading ? "Adding..." : "Create"}
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AddaSalesperson;
