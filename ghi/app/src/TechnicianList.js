import React, { useEffect, useState } from "react";

function TechnicianList() {
    const [technicianListData, setTechnicianListData] = useState([]);

    useEffect(() => {
        async function fetchData() {
            try {
                const url = "http://localhost:8080/api/technicians/";
                const response = await fetch(url);
                if (!response.ok) {
                    alert("Bad response!");
                } else {
                    const data = await response.json();
                    setTechnicianListData(data.technicians);
                }
            } catch (e) {
                alert("Error was raised!")
            }
        }

        fetchData();
    }, []);

    return (
        <div>
            <h1 style={{ fontSize: "42px", paddingTop: "30px", paddingBottom: "10px" }}>Technicians</h1>
            <table className=" table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {technicianListData.map((technicians, index) => (
                        <tr key={index} style={{ backgroundColor: index % 2 === 1 ? "#f2f2f2" : "white" }}>
                            <td>{technicians.employee_id}</td>
                            <td>{technicians.first_name}</td>
                            <td>{technicians.last_name}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default TechnicianList;
